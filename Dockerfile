FROM nginx
RUN mkdir -p /opt/app && rm /etc/nginx/conf.d/default.conf
WORKDIR /opt/app
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY . /opt/app
EXPOSE 80
CMD ["nginx"]
